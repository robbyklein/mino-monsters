// Click Toggle Function
(function($) {
    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
}(jQuery));

$('.menu-icon').click(function(){
  $('.mobile-nav').slideToggle();
})

// Hover
$( ".member" ).hover(
  function() {
    $('.bio').hide()
    $(this).css('transform', 'translateY(-8px)');
    $(this).css('-webkit-transform', 'translateY(-8px)');
    $(this).children('.bio').fadeIn();
  }, function() {
    $(this).css('transform', 'translateY(0)');
    $(this).css('-webkit-transform', 'translateY(0)');
    $(this).children('.bio').fadeOut();
  }
);

// Or Click
$('.member').clickToggle(function() {   
    $('.bio').hide()
    $(this).css('transform', 'translateY(-8px)');
    $(this).css('-webkit-transform', 'translateY(-8px)');
    $(this).children('.bio').fadeIn();
},
function() {
  $('.bio').fadeOut();
  $(this).css('transform', 'translateY(0)');
  $(this).css('-webkit-transform', 'translateY(0)');
});